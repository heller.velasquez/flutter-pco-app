import 'package:flutter_application_pco_test/domain/entities/result.dart';

abstract class AppDatasource {
  Future<Result> getShortUrl(String url);
}
