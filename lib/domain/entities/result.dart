class Result {
  String result_url;
  bool loading;
  bool error;
  String? msgError;
  Result(
      {required this.result_url,
      this.loading = false,
      this.error = false,
      this.msgError});
}
