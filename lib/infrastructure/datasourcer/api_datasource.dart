import 'package:dio/dio.dart';
import 'package:flutter_application_pco_test/domain/datasource/app_datasource.dart';
import 'package:flutter_application_pco_test/domain/entities/result.dart';

class ApiDatasource extends AppDatasource {
  final dio = Dio(BaseOptions(baseUrl: 'https://cleanuri.com/api'));

  @override
  Future<Result> getShortUrl(String url) async {
    String responseError;

    var params = {
      "url": url,
    };

    try {
      final response = await dio.post('/v1/shorten', data: params);

      return Result(result_url: response.data['result_url']);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 400) {
        responseError = error.response?.data['error'];
      } else {
        responseError = "Ocurrio un error en la peticion";
      }

      return Result(result_url: '', error: true, msgError: responseError);
    }
  }
}
