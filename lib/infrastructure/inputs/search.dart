import 'package:formz/formz.dart';

// Define input validation errors
enum SearchError { empty, format }

// Extend FormzInput and provide the input type and error type.
class SearchInput extends FormzInput<String, SearchError> {
  static final RegExp urlRegExp =
      RegExp(r'^(https?://)?' // Protocolo (opcional)
          r'((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|' // Dominio
          r'((\d{1,3}\.){3}\d{1,3}))' // O dirección IP
          r'(:\d+)?' // Puerto (opcional)
          r'(/[-a-z\d%_.~+]*)*' // Ruta
          r'(\?[;&a-z\d%_.~+=-]*)?' // Parámetros de consulta
          r'(\#[-a-z\d_]*)?$' // Fragmento (opcional)
          );

  // Call super.pure to represent an unmodified form input.
  const SearchInput.pure() : super.pure('');

  // Call super.dirty to represent a modified form input.
  const SearchInput.dirty(String value) : super.dirty(value);

  String? get errorMessage {
    if (isValid || isPure) return null;

    if (displayError == SearchError.empty) return 'Este campo es requerido';
    if (displayError == SearchError.format) return 'No tiene formato de URL';

    return null;
  }

  // Override validator to handle validating a given input value.
  @override
  SearchError? validator(String value) {
    if (value.isEmpty || value.trim().isEmpty) return SearchError.empty;
    if (!urlRegExp.hasMatch(value)) return SearchError.format;

    return null;
  }
}
