import 'package:flutter_application_pco_test/domain/datasource/app_datasource.dart';
import 'package:flutter_application_pco_test/domain/entities/result.dart';
import 'package:flutter_application_pco_test/domain/repository/app_repository.dart';

class AppRepositoryImpl extends AppRepository {
  final AppDatasource appDatasource;

  AppRepositoryImpl(this.appDatasource);

  @override
  Future<Result> getShortUrl(String url) {
    return appDatasource.getShortUrl(url);
  }
}
