import 'package:flutter_application_pco_test/infrastructure/inputs/search.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  SearchCubit() : super(const SearchState());

  void onSubmit() {
    emit(state.copyWith(
        formStatus: FormStatus.validating,
        searchInput: SearchInput.dirty(state.searchInput.value),
        isValid: Formz.validate([
          state.searchInput,
        ])));
  }

  void searchChanged(String value) {
    final search = SearchInput.dirty(value);
    emit(
        state.copyWith(searchInput: search, isValid: Formz.validate([search])));
  }
}
