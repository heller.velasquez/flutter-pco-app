part of 'search_cubit.dart';

enum FormStatus { invalid, valid, validating, posting }

class SearchState extends Equatable {
  final FormStatus formStatus;
  final bool isValid;
  final SearchInput searchInput;

  const SearchState(
      {this.formStatus = FormStatus.invalid,
      this.isValid = false,
      this.searchInput = const SearchInput.pure()});

  SearchState copyWith({
    FormStatus? formStatus,
    bool? isValid,
    SearchInput? searchInput,
  }) =>
      SearchState(
        formStatus: formStatus ?? this.formStatus,
        isValid: isValid ?? this.isValid,
        searchInput: searchInput ?? this.searchInput,
      );

  @override
  List<Object> get props => [formStatus, searchInput];
}
