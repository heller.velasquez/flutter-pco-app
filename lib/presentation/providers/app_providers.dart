import 'package:flutter_application_pco_test/domain/entities/result.dart';
import 'package:flutter_application_pco_test/presentation/providers/app_repository_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final getShortUrlProvider = StateNotifierProvider<ApiNotifier, Result>((ref) {
  final fetchShortUrl = ref.watch(appRepositoryProvider).getShortUrl;

  return ApiNotifier(fetchShortUrl: fetchShortUrl);
});

typedef ApiCallback = Future<Result> Function(String url);

class ApiNotifier extends StateNotifier<Result> {
  ApiCallback fetchShortUrl;

  ApiNotifier({required this.fetchShortUrl}) : super(Result(result_url: ""));

  Future<void> getShortUrl(String url) async {
    state = Result(result_url: "", loading: true);
    final Result result = await fetchShortUrl(url);
    result.loading = false;
    state = result;
  }
}
