import 'package:flutter_application_pco_test/infrastructure/datasourcer/api_datasource.dart';
import 'package:flutter_application_pco_test/infrastructure/repository/app_repository_impl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final appRepositoryProvider = Provider((ref) {
  return AppRepositoryImpl(ApiDatasource());
});
