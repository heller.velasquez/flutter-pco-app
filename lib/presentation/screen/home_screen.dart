import 'package:flutter/material.dart';
import 'package:flutter_application_pco_test/presentation/bloc/cubit/search_cubit.dart';
import 'package:flutter_application_pco_test/presentation/providers/app_providers.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';

class HomeView extends ConsumerStatefulWidget {
  const HomeView({super.key});

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends ConsumerState<HomeView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SHORT URL APP'),
      ),
      body: BlocProvider(
        create: (context) => SearchCubit(),
        child: const SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: _FormView(),
        )),
      ),
    );
  }
}

class _FormView extends ConsumerStatefulWidget {
  const _FormView();

  @override
  _FormViewState createState() => _FormViewState();
}

class _FormViewState extends ConsumerState<_FormView> {
  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<SearchCubit>();

    final search = cubit.state.searchInput;

    final apiResponse = ref.watch(getShortUrlProvider);

    return Column(children: [
      const SizedBox(height: 20),
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 4,
            child: TextFormField(
              onChanged: (value) => cubit.searchChanged(value),
              decoration: InputDecoration(
                errorText: search.errorMessage ?? apiResponse.msgError,
                label: const Text('Search'),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: FilledButton.icon(
              onPressed: cubit.state.isValid
                  ? () {
                      ref
                          .read(getShortUrlProvider.notifier)
                          .getShortUrl(search.value);
                    }
                  : () {
                      cubit.onSubmit();
                    },
              icon: const Icon(Icons.send),
              label: const Text('Acortar'),
            ),
          ),
        ],
      ),
      const SizedBox(height: 20),
      Visibility(
        visible: apiResponse.loading,
        child: LottieBuilder.asset(
          'assets/lottie/loading.json',
          width: 70,
          height: 70,
        ),
      ),
      Visibility(
        visible: !apiResponse.loading,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blue),
            borderRadius: BorderRadius.circular(10.0),
          ),
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              const Text(
                'Short URL: ',
                style: TextStyle(color: Colors.blue),
              ),
              Text(apiResponse.result_url),
            ],
          ),
        ),
      )
    ]);
  }
}
