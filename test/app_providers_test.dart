import 'package:flutter_application_pco_test/presentation/providers/app_providers.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_application_pco_test/domain/entities/result.dart';

void main() {
  late ApiNotifier apiNotifier;

  setUp(() {
    apiNotifier = ApiNotifier(fetchShortUrl: (url) async {
      return Result(result_url: "https://shorturl.com", loading: false);
    });
  });

  group('ApiNotifier', () {
    test('getShortUrl sets loading to false', () async {
      await apiNotifier.getShortUrl('https://example.com');

      expect(apiNotifier.state.loading, false);
    });

    test('getShortUrl updates the state with the result', () async {
      await apiNotifier.getShortUrl('https://example.com');

      expect(apiNotifier.state.result_url, 'https://shorturl.com');
    });
  });
}
